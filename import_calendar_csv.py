import panchanga
import pandas as pd
import datetime

dates = pd.read_csv('resources/festivals.txt', names=['date', 'event'])
dates

def date_to_tithi(date):
    date = panchanga.Date(int(str(date)[:4]), int(str(date)[4:6]), int(str(date)[6:]))
    place = panchanga.Place(45.565300, -122.644800, -8)
    return panchanga.masa(panchanga.gregorian_to_jd(date), place)[0], panchanga.tithi(panchanga.gregorian_to_jd(date), place)[0]

dates['tithi'] = dates.date.apply(date_to_tithi)
dates['year'] = dates.date.apply(lambda x: int(str(x)[:4]))
dates['month'] = dates.date.apply(lambda x: int(str(x)[4:6]))
dates['day'] = dates.date.apply(lambda x: int(str(x)[6:]))

dates = dates.set_index(['year', 'month', 'day'])
dates

today = datetime.datetime.now()
year, month, day = today.year, today.month, today.day

futureday = today+datetime.timedelta(days=1)
events = dates.loc[(futureday.year, futureday.month, futureday.day)]
events



initial = futureday.strftime('%a')+': '
strings = []
events.apply(lambda row: strings.append(initial+str(row.event)), axis=1)
print('\n'.join(strings))
def add_textbox(text):
    print(text.event)
    textline = futureday.strftime('%a')+': '+text.event
    print(textline)
events.apply(add_textbox, axis=1)
