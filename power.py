from gpiozero import LED

def blink(pin):
    try:
        btn = LED(pin, active_high=False, initial_value=False)
        btn.blink(on_time=0.01, off_time=0.01, n=1, background=False)
        btn.close()
    except:
        print 'Pin already active.'
    return

blink(19)
