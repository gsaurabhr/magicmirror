# smartmirror.py
# requirements
# requests, feedparser, traceback, PIL

from Tkinter import *
import locale
import threading
import time
import datetime
import requests
import json
import traceback
import feedparser
import os

from os import path
from random import randint
from glob import glob
from PIL import Image, ImageTk
from contextlib import contextmanager
from bs4 import BeautifulSoup
import dropbox
import Adafruit_DHT
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import pandas as pd

sensor = Adafruit_DHT.AM2302
DHTpin = 2

LOCALE_LOCK = threading.Lock()

ui_locale = '' # e.g. 'fr_FR' fro French, '' as default for 'en_US
time_format = 24 # 12 or 24
date_format = "%b %d, %Y" # check python doc for strftime() for options
try:
    with open('./data/locationapi.key', 'r') as f:
        location_api_token = f.readlines()[0].strip('\n')
    print location_api_token
except:
    print 'Could not find location api key file'
    print 'Create an account at https://ipstack.com and create a file at ./data/locationapi.key with the api key value written in it'
try:
    with open('./data/weatherapi.key', 'r') as f:
        weather_api_token = f.readlines()[0].strip('\n')
    print weather_api_token
except:
    print 'Could not find weather api key file'
    print 'Create an account at https://darksky.net/dev/ and create a file at ./data/weatherapi.key with the api key value written in it'
try:
    with open('./data/dropboxapi.key', 'r') as f:
        dropbox_api_token = f.readlines()[0].strip('\n')
except:
    print 'Could not find dropbox api key file'
    print 'Create a developer account and create a file at ./data/dropboxapi.key with the api key value written in it'
weather_lang = 'en' # see https://darksky.net/dev/docs/forecast for full list of language parameters values
weather_unit = 'si' # see https://darksky.net/dev/docs/forecast for full list of unit parameters values
latitude = None # Set this if IP location lookup does not work for you (must be a string)
longitude = None # Set this if IP location lookup does not work for you (must be a string)
xlarge_text_size = 50
large_text_size = 35
medium_text_size = 20
small_text_size = 14
rot = {1 : 0, 3 : 180, 6 : 270, 8 : 90}
imageHeight = 350
picfolder = './data/photos/'
picfolderx = './data/photos/transformed%d/'%imageHeight
if not path.isdir(picfolderx):
    os.mkdir(picfolderx)

nbAgencies = ['mit']
nbRoutes = {'mit' : ['tech', 'saferidecampshut']}
nbNames = {'tech' : 'Tech Shuttle', 'saferidecampshut' : 'Saferide Campus Shuttle'}
nbStops = {'mit' : 'tangwest'}

@contextmanager
def setlocale(name): # thread proof function to work with locale
    with LOCALE_LOCK:
        saved = locale.setlocale(locale.LC_ALL)
        try:
            yield locale.setlocale(locale.LC_ALL, name)
        finally:
            locale.setlocale(locale.LC_ALL, saved)

# maps open weather icons to darksky api icon names
icon_lookup = {
    'clear-day': "resources/weather_icons/clear-day.png",  # clear sky day
    'wind': "resources/weather_icons/wind.png",   # wind
    'cloudy': "resources/weather_icons/cloudy.png",  # cloudy
    'partly-cloudy-day': "resources/weather_icons/partly-cloudy-day.png",  # partly cloudy day
    'rain': "resources/weather_icons/rain.png",  # rain day
    'snow': "resources/weather_icons/snow.png",  # snow day
    'snow-thin': "resources/weather_icons/snow.png",  # sleet day
    'fog': "resources/weather_icons/fog.png",  # fog day
    'clear-night': "resources/weather_icons/clear-night.png",  # clear sky night
    'partly-cloudy-night': "resources/weather_icons/partly-cloudy-night.png",  # scattered clouds night
    'thunderstorm': "resources/weather_icons/thunderstorm.png",  # thunderstorm
    'tornado': "resources/weather_icons/tornado.png",    # tornado
    'hail': "resources/weather_icons/hail.png"  # hail
}

class Clock(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        # initialize time label
        self.time1 = ''
        self.timeLbl = Label(self, font=('Helvetica', large_text_size), fg="white", bg="black")
        self.timeLbl.pack(side=TOP, anchor=E)
        # initialize day of week
        self.day_of_week1 = ''
        self.dayOWLbl = Label(self, text=self.day_of_week1, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.dayOWLbl.pack(side=TOP, anchor=E)
        # initialize date label
        self.date1 = ''
        self.dateLbl = Label(self, text=self.date1, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.dateLbl.pack(side=TOP, anchor=E)
        self.tick()

    def tick(self):
        with setlocale(ui_locale):
            if time_format == 12:
                time2 = time.strftime('%I:%M %p') #hour in 12h format
            else:
                time2 = time.strftime('%H:%M') #hour in 24h format

            day_of_week2 = time.strftime('%A')
            date2 = time.strftime(date_format)
            # if time string has changed, update it
            if time2 != self.time1:
                self.time1 = time2
                self.timeLbl.config(text=time2)
            if day_of_week2 != self.day_of_week1:
                self.day_of_week1 = day_of_week2
                self.dayOWLbl.config(text=day_of_week2)
            if date2 != self.date1:
                self.date1 = date2
                self.dateLbl.config(text=date2)
            # calls itself every 200 milliseconds
            # to update the time display as needed
            # could use >200 ms, but display gets jerky
            self.timeLbl.after(200, self.tick)

class Weather(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        self.temperature = ''
        self.forecast = ''
        self.location = ''
        self.currently = ''
        self.icon = ''
        self.degreeFrm = Frame(self, bg="black")
        self.degreeFrm.pack(side=TOP, anchor=W)
        self.temperatureLbl = Label(self.degreeFrm, font=('Helvetica', xlarge_text_size), fg="white", bg="black")
        self.temperatureLbl.pack(side=LEFT, anchor=N)
        self.iconLbl = Label(self.degreeFrm, bg="black")
        self.iconLbl.pack(side=LEFT, anchor=N, padx=20)
        self.currentlyLbl = Label(self, font=('Helvetica', medium_text_size), fg="white", bg="black")
        self.currentlyLbl.pack(side=TOP, anchor=W)
        
        self.forecastFrm = Frame(self, bg="black")
        self.forecastFrm.pack(side=TOP, anchor=W)
        self.forecastRoll, self.timeRoll, self.iconRoll, self.precRoll, self.tempRoll = [], [], [], [], []
        for i in range(7):
            self.forecastRoll.append(Frame(self.forecastFrm, bg="black"))
            self.forecastRoll[-1].pack(side=LEFT, anchor=W)
            self.timeRoll.append(Label(self.forecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.timeRoll[-1].pack(side=BOTTOM)
            self.tempRoll.append(Label(self.forecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.tempRoll[-1].pack(side=BOTTOM)
            self.iconRoll.append(Label(self.forecastRoll[-1], bg="black"))
            self.iconRoll[-1].pack(side=BOTTOM, padx=10)
            self.precRoll.append(Label(self.forecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.precRoll[-1].pack(side=BOTTOM)
        #self.forecastLbl = Label(self, font=('Helvetica', small_text_size), fg="white", bg="black")
        #self.forecastLbl.pack(side=TOP, anchor=W)
        #self.locationLbl = Label(self, font=('Helvetica', small_text_size), fg="white", bg="black")
        #self.locationLbl.pack(side=TOP, anchor=W)
        self.get_weather()

    def get_ip(self):
        try:
            ip_url = "http://jsonip.com/"
            req = requests.get(ip_url, verify=False)
            ip_json = json.loads(req.text)
            return ip_json['ip']
        except Exception as e:
            traceback.print_exc()
            return "Error: %s. Cannot get ip." % e

    def get_weather(self):
        try:
            if latitude is None and longitude is None:
                # get location
                location_req_url = "http://api.ipstack.com/%s?access_key=%s" % (self.get_ip(), location_api_token)
                r = requests.get(location_req_url)
                location_obj = json.loads(r.text)

                lat = location_obj['latitude']
                lon = location_obj['longitude']

                location2 = "%s, %s" % (location_obj['city'], location_obj['region_code'])

                # get weather
                weather_req_url = "https://api.darksky.net/forecast/%s/%s,%s?lang=%s&units=%s" % (weather_api_token, lat,lon,weather_lang,weather_unit)
            else:
                location2 = ""
                # get weather
                weather_req_url = "https://api.darksky.net/forecast/%s/%s,%s?lang=%s&units=%s" % (weather_api_token, latitude, longitude,
                                                                                                  weather_lang, weather_unit)

            r = requests.get(weather_req_url)
            weather_obj = json.loads(r.text)

            degree_sign= u'\N{DEGREE SIGN}'
            temperature2 = "%s%s" % (str(int(weather_obj['currently']['temperature'])), degree_sign)
            currently2 = weather_obj['currently']['summary']
            forecast2 = weather_obj["hourly"]["summary"]
            
            tps = [1, 2, 3, 5, 7, 10, 13]
            times, icons, precp, temps = [], [], [], []
            for t in tps:
                dic = weather_obj['hourly']['data'][t]
                times.append(int(time.localtime(dic['time']).tm_hour))
                if times[-1] > 12:
                    times[-1] = '%d PM'%(times[-1]-12)
                else:
                    times[-1] = '%d AM'%times[-1]
                icons.append(dic['icon'])
                precp.append(round(dic['precipProbability']*10)*10)
                if precp[-1] > 30:
                    precp[-1] = '%d %%'%(precp[-1])
                else:
                    precp[-1] = ''
                temps.append(int(round(dic['temperature'])))

            icon_id = weather_obj['currently']['icon']
            icon2 = None

            if icon_id in icon_lookup:
                icon2 = icon_lookup[icon_id]

            if icon2 is not None:
                if self.icon != icon2:
                    self.icon = icon2
                    image = Image.open(icon2)
                    image = image.resize((100, 100), Image.ANTIALIAS)
                    image = image.convert('RGB')
                    photo = ImageTk.PhotoImage(image)

                    self.iconLbl.config(image=photo)
                    self.iconLbl.image = photo
            else:
                # remove image
                self.iconLbl.config(image='')

            if self.currently != currently2:
                self.currently = currently2
                self.currentlyLbl.config(text=currently2)
            #if self.forecast != forecast2:
            #    self.forecast = forecast2
            #    self.forecastLbl.config(text=forecast2)
            if self.temperature != temperature2:
                self.temperature = temperature2
                self.temperatureLbl.config(text=temperature2)
            #if self.location != location2:
            #    if location2 == ", ":
            #        self.location = "Cannot Pinpoint Location"
            #        self.locationLbl.config(text="Cannot Pinpoint Location")
            #    else:
            #        self.location = location2
            #        self.locationLbl.config(text=location2)
            for i in range(len(times)):
                self.timeRoll[i].config(text=times[i])
                self.tempRoll[i].config(text=temps[i])
                self.precRoll[i].config(text=precp[i])
                icon, icon2 = icons[i], None
                if icon in icon_lookup:
                    icon2 = icon_lookup[icon]
                if icon2 is not None:
                    image = Image.open(icon2)
                    image = image.resize((35, 35), Image.ANTIALIAS)
                    image = image.convert('RGB')
                    photo = ImageTk.PhotoImage(image)

                    self.iconRoll[i].config(image=photo)
                    self.iconRoll[i].image = photo
        except Exception as e:
            traceback.print_exc()
            print "Error: %s. Cannot get weather." % e

        self.after(600000, self.get_weather)

    @staticmethod
    def convert_kelvin_to_fahrenheit(kelvin_temp):
        return 1.8 * (kelvin_temp - 273) + 32

class InfoPane(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.textContainer = Frame(self, bg="black")
        self.textContainer.pack(side=TOP)
        self.get_text()
    
    def get_text(self):
        try:
            # remove all children
            for widget in self.textContainer.winfo_children():
                widget.destroy()
            # MIT shuttle schedule
            for agency in nbAgencies:
                for route in nbRoutes[agency]:
                    text = nbNames[route] + ': '
                    predStr = 'http://webservices.nextbus.com/service/publicJSONFeed?command=predictions&a=%s&r=%s&s=%s'%(agency, route, nbStops[agency])
                    predjson = requests.get(predStr)
                    pred = json.loads(predjson.content)
                    if 'direction' in pred['predictions'].keys():
                        if len(pred['predictions']['direction']['prediction']) > 0:
                            text = text + pred['predictions']['direction']['prediction'][0]['minutes']
                            if len(pred['predictions']['direction']['prediction']) > 1:
                                text = text + ', ' + pred['predictions']['direction']['prediction'][1]['minutes']
                            text += 'mins'
                    else:
                        text += 'Not running'
                    textline = InfoFrame(self.textContainer, text)
                    textline.pack(side=TOP, anchor=E)
                        
        except Exception as e:
            traceback.print_exc()
            print "Error: %s. Cannot get text/shuttle predictions." % e

        self.after(20000, self.get_text)

class InfoFrame(Frame):
    def __init__(self, parent, event_name=""):
        Frame.__init__(self, parent, bg='black')

        self.eventName = event_name
        self.eventNameLbl = Label(self, text=self.eventName, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.eventNameLbl.pack(side=RIGHT, anchor=E)

class PicturePane(Frame):
    def __init__(self, parent, *args, **kwargs):
        self.dbx = dropbox.Dropbox(dropbox_api_token)
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.pictureContainer = Frame(self, bg="black")
        self.pictureContainer.pack(side=BOTTOM)
        self.get_pictures()

    def get_pictures(self):
        try:
            # remove all children
            for widget in self.pictureContainer.winfo_children():
                widget.destroy()
            dbx = self.dbx
            files = dbx.files_list_folder('/MirrorPics')
            remote_files = [f.name for f in files.entries]
            local_files = [path.basename(f) for f in glob('./data/photos/*') if not path.isdir(f)]
            for f in remote_files:
                if f not in local_files:
                    dbx.files_download_to_file('./data/photos/%s'%f, '/MirrorPics/%s'%f)
                    print 'Downloaded %s'%f
            for f in local_files:
                if f not in remote_files:
                    os.remove('./data/photos/%s'%f)
                    print 'Removed %s'%f
            
            pics = [f for f in glob(picfolder+'*') if not path.isdir(f)]
            indices = [0, 0]
            while (indices[0] == indices[1]):
                indices = [randint(0, len(pics)) for x in range(2)]
            
            for i in range(len(indices)):
                picture = PictureFrame(self.pictureContainer, pics[indices[i]])
                picture.pack(side=LEFT)
        
        except Exception as e:
            traceback.print_exc()
            print "Error: %s. Cannot get pictures." % e

        self.after(600*1000, self.get_pictures)

class PictureFrame(Frame):
    def __init__(self, parent, pic=""):
        Frame.__init__(self, parent, bg='black')
        
        fname = path.basename(pic)
        if path.join(picfolderx, fname) in glob(picfolderx+'*'):
            image = Image.open(path.join(picfolderx, fname))
        else:
            image = Image.open(pic)
            ori = image._getexif().get(274, 1) # 274 is the exif tag for orientation; get this data before transforming the image
            w, h = image.size
            wnew, hnew = int(float(w*imageHeight/h)), imageHeight
            image = image.resize((wnew, hnew), Image.ANTIALIAS)
            image = image.rotate(rot.get(ori, 0))
            image = image.convert('RGB')
            image.save(path.join(picfolderx, fname))
        photo = ImageTk.PhotoImage(image)

        self.picLbl = Label(self, bg="black")
        self.picLbl.config(image=photo)
        self.picLbl.image = photo
        self.picLbl.pack(side=LEFT, padx=10)

class THFrame(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.labelframe = Frame(self, bg='black')
        self.labelframe.pack(side=RIGHT)
        self.Tlabel = Label(self.labelframe, text="Temperature: ", font=('Helvetica', small_text_size), fg="lawn green", bg="black", anchor=W)
        self.Hlabel = Label(self.labelframe, text="Humidity: ", font=('Helvetica', small_text_size), fg="cyan", bg="black", anchor=W)
        self.Tlabel.pack(side=TOP, anchor=W)
        self.Hlabel.pack(side=BOTTOM, anchor=W)
        self.figure = plt.Figure(figsize=(22, 0.7), dpi=100, facecolor='black')
        self.ax = self.figure.add_subplot(1, 1, 1)
        self.ax.patch.set_facecolor('black')
        self.ax2 = self.ax.twinx()
        self.figcanvas = FigureCanvasTkAgg(self.figure, self)
        self.figcanvas.get_tk_widget().pack(side=LEFT)
        self.figcanvas.get_tk_widget().config(bg='black')
        self.read_thlog()
        self.getTH()

    def read_thlog(self):
        try:
            self.n = 0
            with open('./th.size', 'r') as f:
                self.n = int(f.readline().strip('\n'))
            self.thdata = pd.read_csv('th.log', skiprows=range(1, max(0, self.n-1440)), index_col=0, parse_dates=[0], dtype={1 : float, 2 : float})
            #print self.thdata
        except Exception as e:
            traceback.print_exc()
            print "Error: %s. Error reading sensor log file" % e
            self.thdata = None

    def getTH(self):
        try:
            humidity, temperature = Adafruit_DHT.read_retry(sensor, DHTpin)
            self.n += 1
            ctim = datetime.datetime.now()
            self.Tlabel.config(text='Temperature: %.0f *C'%temperature)
            self.Hlabel.config(text='Humidity: %.2f%%'%humidity)
            print '%s: %.2f RH, %.2fC'%(ctim, humidity, temperature)
            with open('./th.log', 'a') as f:
                f.writelines('%s, %.4f, %.4f\n'%(ctim, temperature, humidity))
            with open('./th.size', 'w') as f:
                f.writelines('%d'%self.n)
            # read csv into dataframe
            # discard topmost entry, and new entry to the bottom
            # save dataframe and then plot it
            try:
                self.thdata.drop(index=[self.thdata.index[0]])
                df = pd.DataFrame(index=[pd.to_datetime(ctim)], columns=['Temperature', 'Humidity'], data=[[temperature, humidity]])
                self.thdata = self.thdata.append(df)
                self.ax.clear()
                self.ax2.clear()
                self.ax2.patch.set_facecolor('black')
                self.ax.patch.set_facecolor('black')
                #print self.thdata['Humidity'] < 100
                self.thdata['Temperature'][self.thdata['Humidity'] < 100].plot(c='lime', ax=self.ax, lw=0.75)
                self.thdata['Humidity'][self.thdata['Humidity'] < 100].plot(c='cyan', ax=self.ax2, lw=0.75)
                self.ax.tick_params(axis='y', colors='lime')
                self.ax2.tick_params(axis='y', colors='cyan')
                #self.ax.tick_params(axis='x', colors='white')
                self.ax.set_ylim(16, 32)
                self.ax2.set_ylim(10, 60)
                plt.setp(self.ax, yticks=[16, 24, 32])
                plt.setp(self.ax2, yticks=[20, 40, 60])
                self.figcanvas.draw()
                #plt.setp(self.ax)
            except Exception as e:
                traceback.print_exc()
                print 'Error while plotting.'
        except Exception as e:
            traceback.print_exc()
            print "Error: %s. Cannot read sensor data." % e

        self.after(60000, self.getTH)

class BottomPane(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.title = ''
        self.textLbl = Label(self, text=self.title, font=('Helvetica', medium_text_size), fg="white", bg="black")
        self.textLbl.pack(side=TOP, anchor=W)
        self.textContainer = Frame(self, bg="black")
        self.textContainer.pack(side=TOP)
        self.get_text()

    def get_text(self):
        try:
            # remove all children
            for widget in self.textContainer.winfo_children():
                widget.destroy()
           # if news_country_code == None:
           #     headlines_url = "https://news.google.com/news?ned=us&output=rss"
           # else:
           #     headlines_url = "https://news.google.com/news?ned=%s&output=rss" % news_country_code
            textUrl = "http://docs.google.com/document/export?format=txt&id=1nzzxIMl9TpXi6JRtPbd42TzUYpj7xW9Q23jz5VoTJho&hl=en#"

            contents = requests.get(textUrl)
            lines = contents.content.decode("utf-8-sig").encode("utf-8").split('\r\n')

            for post in lines[-5:]:
                text = post
                textline = BottomText(self.textContainer, text)
                textline.pack(side=TOP, anchor=W)
        
        except Exception as e:
            traceback.print_exc()
            print "Error: %s. Cannot get text/shuttle predictions." % e

        self.after(5000, self.get_text)

class BottomText(Frame):
    def __init__(self, parent, event_name=""):
        Frame.__init__(self, parent, bg='black')

        self.eventName = event_name
        self.eventNameLbl = Label(self, text=self.eventName, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.eventNameLbl.pack(side=LEFT, anchor=N)

class Calendar(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        self.title = 'Calendar Events'
        self.calendarLbl = Label(self, text=self.title, font=('Helvetica', medium_text_size), fg="white", bg="black")
        self.calendarLbl.pack(side=TOP, anchor=E)
        self.calendarEventContainer = Frame(self, bg='black')
        self.calendarEventContainer.pack(side=TOP, anchor=E)
        self.get_events()

    def get_events(self):
        #TODO: implement this method
        # reference https://developers.google.com/google-apps/calendar/quickstart/python

        # remove all children
        for widget in self.calendarEventContainer.winfo_children():
            widget.destroy()

        calendar_event = CalendarEvent(self.calendarEventContainer)
        calendar_event.pack(side=TOP, anchor=E)
        pass

class CalendarEvent(Frame):
    def __init__(self, parent, event_name="Event 1"):
        Frame.__init__(self, parent, bg='black')
        self.eventName = event_name
        self.eventNameLbl = Label(self, text=self.eventName, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.eventNameLbl.pack(side=TOP, anchor=E)

class FullscreenWindow:

    def __init__(self):
        self.tk = Tk()
        self.tk.configure(background='black', cursor='none')
        self.topFrame = Frame(self.tk, background = 'black')
        self.bottomFrame = Frame(self.tk, background = 'black')
        self.topFrame.pack(side = TOP, fill=BOTH, expand = YES)
        self.bottomFrame.pack(side = BOTTOM, fill=BOTH, expand = YES)
        self.state = False
        self.tk.bind("<Return>", self.toggle_fullscreen)
        self.tk.bind("<Escape>", self.end_fullscreen)
        # information pane
        # left (weather)
        # weather
        self.weather = Weather(self.topFrame)
        self.weather.pack(side=LEFT, anchor=N, padx=0, pady=0)
        # right (clock, shuttle schedule)
        self.rightInfoFrm = Frame(self.topFrame, bg="black")
        self.rightInfoFrm.pack(side=RIGHT, anchor=N)
        # clock
        self.clock = Clock(self.rightInfoFrm)
        self.clock.pack(side=TOP, anchor=E, padx=0, pady=0)
        # info
        self.info = InfoPane(self.rightInfoFrm)
        self.info.pack(side=TOP, padx=0, pady=10)
        # bs
        self.thframe = THFrame(self.bottomFrame)
        self.thframe.pack(side=BOTTOM, anchor=S)
        self.pics = PicturePane(self.bottomFrame)
        self.pics.pack(side=BOTTOM, anchor=S)
        # calender
        # self.calender = Calendar(self.bottomFrame)
        # self.calender.pack(side = RIGHT, anchor=S, padx=100, pady=60)
        
        # start with full screen
        self.toggle_fullscreen()

    def toggle_fullscreen(self, event=None):
        self.state = not self.state  # Just toggling the boolean
        self.tk.attributes("-fullscreen", self.state)
        return "break"

    def end_fullscreen(self, event=None):
        self.state = False
        self.tk.attributes("-fullscreen", False)
        return "break"

if __name__ == '__main__':
    w = FullscreenWindow()
    w.tk.mainloop()
