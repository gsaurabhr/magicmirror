# -*- coding: UTF-8 -*-
#
# smartmirror.py
# requirements
# requests, feedparser, traceback, PIL
import os
from os import path
from collections import namedtuple as struct
from random import randint
from glob import glob
import json
import locale
import time
import datetime
import sys
import importlib
import panchanga
import io
if sys.version_info.major==3:
    _python = 3
else:
    _python = 2

if _python==3:
    from tkinter import *
    from tkinter import ttk
else:
    from Tkinter import *
    import ttk

import requests
import traceback
from PIL import Image, ImageTk
# import feedparser
# import dropbox
try:
    import Adafruit_DHT
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
    dh = True
except:
    dh = False
import pandas as pd

show_info_pane = True
show_pictures_frame = False

if dh:
    sensor = Adafruit_DHT.AM2302
    DHTpin = 2

if show_pictures_frame:
    import dropbox

Place = struct('Location', ['latitude', 'longitude', 'timezone'])
Date = struct('Date', ['year', 'month', 'day'])

ui_locale = '' # e.g. 'fr_FR' for French, '' as default
locale.setlocale(locale.LC_ALL, ui_locale)
print('check 0')
default_location_info = {
    'latitude':'45.565300',
    'longitude':'-122.644800',
    'timezone':'America/Los_Angeles',
    'utc_offset':-8
}
location_info = None
weather_detail = None
weather_daily = None
moon_info = None

time_format = 24 # 12 or 24
date_format = "%b %d, %Y" # check python doc for strftime() for options
weather_lang = 'en'
weather_unit = 'metric'
degree_sign= u'\N{DEGREE SIGN}'

xlarge_text_size = 50
large_text_size = 35
medium_text_size = 20
small_text_size = 14
tiny_text_size = 9

dates = pd.read_csv('resources/festivals.txt', names=['date', 'event'])
dates['year'] = dates.date.apply(lambda x: int(str(x)[:4]))
dates['month'] = dates.date.apply(lambda x: int(str(x)[4:6]))
dates['day'] = dates.date.apply(lambda x: int(str(x)[6:]))
dates = dates.set_index(['year', 'month', 'day'])

try:
    with open('./data/keys.key', 'r') as f:
        apikeys = json.load(f)
    print('Loaded api keys')
except:
    print('Could not find api keys file at data/keys.key')
print(apikeys)

# panchanga lookup table
with io.open('resources/sanskrit_names.json', 'r', encoding='utf-8') as f:
    pvalue_lookup = json.load(f)
pvalue_lookup

# map weather codes to actual information
value_lookup = {
    "epaHealthConcern": {
        "0": "Good",
        "1": "Moderate",
        "2": "Unhealthy for Sensitive Groups",
        "3": "Unhealthy",
        "4": "Very Unhealthy",
        "5": "Hazardous"
    },
    "epaPrimaryPollutant": {
        "0": "PM2.5",
        "1": "PM10",
        "2": "O3",
        "3": "NO2",
        "4": "CO",
        "5": "SO2"
    },
    "precipitationType": {
        "0": "N/A",
        "1": "Rain",
        "2": "Snow",
        "3": "Freezing Rain",
        "4": "Ice Pellets"
    },
    "weatherCode": {
        "0": "Unknown",
        "1000": "Clear",
        "1001": "Cloudy",
        "1100": "Mostly Clear",
        "1101": "Partly Cloudy",
        "1102": "Mostly Cloudy",
        "2000": "Fog",
        "2100": "Light Fog",
        "3000": "Light Wind",
        "3001": "Wind",
        "3002": "Strong Wind",
        "4000": "Drizzle",
        "4001": "Rain",
        "4200": "Light Rain",
        "4201": "Heavy Rain",
        "5000": "Snow",
        "5001": "Flurries",
        "5100": "Light Snow",
        "5101": "Heavy Snow",
        "6000": "Freezing Drizzle",
        "6001": "Freezing Rain",
        "6200": "Light Freezing Rain",
        "6201": "Heavy Freezing Rain",
        "7000": "Ice Pellets",
        "7101": "Heavy Ice Pellets",
        "7102": "Light Ice Pellets",
        "8000": "Thunderstorm"
    },
    "weatherIcon" : {
        "0": ["Unknown", "Unknown"],
        "1000": ["resources/weather_icons/clear-day.png", "resources/weather_icons/clear-night.png"],
        "1001": ["resources/weather_icons/cloudy.png", "resources/weather_icons/cloudy.png"],
        "1100": ["resources/weather_icons/clear-day.png", "resources/weather_icons/clear-night.png"],
        "1101": ["resources/weather_icons/partly-cloudy-day.png", "resources/weather_icons/partly-cloudy-night.png"],
        "1102": ["resources/weather_icons/cloudy.png", "resources/weather_icons/cloudy.png"],
        "2000": ["resources/weather_icons/fog.png", "resources/weather_icons/Fog.png"],
        "2100": ["resources/weather_icons/fog.png", "resources/weather_icons/fog.png"],
        "3000": ["resources/weather_icons/wind.png", "resources/weather_icons/wind.png"],
        "3001": ["resources/weather_icons/wind.png", "resources/weather_icons/wind.png"],
        "3002": ["resources/weather_icons/wind.png", "resources/weather_icons/wind.png"],
        "4000": ["resources/weather_icons/rain.png", "resources/weather_icons/rain.png"],
        "4001": ["resources/weather_icons/rain.png", "resources/weather_icons/rain.png"],
        "4200": ["resources/weather_icons/rain.png", "resources/weather_icons/rain.png"],
        "4201": ["resources/weather_icons/rain.png", "resources/weather_icons/rain.png"],
        "5000": ["resources/weather_icons/snow.png", "resources/weather_icons/snow.png"],
        "5001": ["resources/weather_icons/snow.png", "resources/weather_icons/snow.png"],
        "5100": ["resources/weather_icons/snow.png", "resources/weather_icons/snow.png"],
        "5101": ["resources/weather_icons/snow.png", "resources/weather_icons/snow.png"],
        "6000": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "6001": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "6200": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "6201": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "7000": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "7101": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "7102": ["resources/weather_icons/hail.png", "resources/weather_icons/hail.png"],
        "8000": ["resources/weather_icons/thunderstorm.png", "resources/weather_icons/thunderstorm.png"]
        }
  }

print('Check 1')
def get_ip():
    try:
        ip_url = "http://jsonip.com/"
        r = requests.get(ip_url, verify=True, timeout=1)
        ip_json = json.loads(r.text)
        return ip_json['ip']
    except Exception as e:
        print('Timeout occurred')
        traceback.print_exc()
        return "Error: %s. Cannot get ip." % e
print(get_ip())
print(location_info)
def update_location():
    global location_info
    try:
        loc_url = 'https://ipapi.co/%s/json/'%get_ip()#f"https://ipapi.co/{get_ip()}/json/"
        r = requests.get(loc_url, verify=True, timeout=1)
        _location_info = json.loads(r.text)
        if not _location_info.get('error', False):
            location_info = _location_info
        else:
            # print(_location_info)
            location_info = default_location_info
        location_info['place'] = Place(
            float(location_info['latitude']),
            float(location_info['longitude']),
            float(location_info['utc_offset'])/100
        )
        location_info['date'] = panchanga.gregorian_to_jd(Date(
            datetime.datetime.now().year,
            datetime.datetime.now().month,
            datetime.datetime.now().day,
        ))
        return
    except Exception as e:
        print('Timeout occurred')
        traceback.print_exc()
        location_info['place'] = Place(
            float(location_info['latitude']),
            float(location_info['longitude']),
            float(location_info['utc_offset'])/100
        )
        location_info['date'] = panchanga.gregorian_to_jd(Date(
            datetime.datetime.now().year,
            datetime.datetime.now().month,
            datetime.datetime.now().day,
        ))

        return "Error: %s. Cannot get ip." % e
update_location()
print(location_info)

#---------------------------------------------------------------------
masa = pvalue_lookup['masas'][str(panchanga.masa(location_info['date'], location_info['place'])[0])]
tithi = pvalue_lookup['tithis'][str(panchanga.tithi(location_info['date'], location_info['place'])[0])]
masa, tithi
#---------------------------------------------------------------------

def update_weather():
    global weather_daily, weather_detail
    update_location()
    weather_url = "https://data.climacell.co/v4/timelines"
    querystring = {
        "location":'%s,%s'%(location_info['latitude'], location_info['longitude']),#f"{location_info['latitude']},{location_info['longitude']}",
        "fields":[
            "temperature", "temperatureApparent", "windSpeed", "windDirection","cloudCover",
            "precipitationIntensity", "precipitationProbability", "precipitationType",
            "weatherCode", "epaIndex", "epaPrimaryPollutant", "epaHealthConcern"
        ],
        "timesteps":["1h"],
        "units":weather_unit,
        "timezone":location_info['timezone'],
        "apikey":apikeys['weather']
    }
    weather_detail = json.loads(
        requests.request("GET", weather_url, params=querystring).text
    )
    querystring = {
        "location":'%s,%s'%(location_info['latitude'], location_info['longitude']),#f"{location_info['latitude']},{location_info['longitude']}",
        "fields":[
            "weatherCode", "sunriseTime", "sunsetTime", "precipitationProbability",
            "temperatureMax", "temperatureMin"
        ],
        "timesteps":["1d"],
        "units":weather_unit,
        "timezone":location_info['timezone'],
        "apikey":apikeys['weather']
    }
    weather_daily = json.loads(
        requests.request("GET", weather_url, params=querystring).text
    )
    return
update_weather()
weather_daily
weather_detail

def get_temperature_str(weather_packet):
    return '%.0f%s'%(weather_detail['data']['timelines'][0]['intervals'][0]['values']['temperature'], degree_sign)
def get_summary_str(weather_packet):
    return value_lookup["weatherCode"][str(weather_packet['weatherCode'])]
def get_time_as_dt(timestr):
    return datetime.datetime.strptime(timestr[:-6], '%Y-%m-%dT%H:%M:%S')
def get_icon_path(weather_packet):
    return value_lookup["weatherIcon"][str(weather_packet['weatherCode'])]
get_temperature_str(weather_detail['data']['timelines'][0]['intervals'][0]['values'])
get_summary_str(weather_detail['data']['timelines'][0]['intervals'][0]['values'])
get_time_as_dt(weather_detail['data']['timelines'][0]['intervals'][0]['startTime'])
get_icon_path(weather_detail['data']['timelines'][0]['intervals'][0]['values'])
get_time_as_dt(weather_daily['data']['timelines'][0]['intervals'][0]['values']['sunriseTime'])

weather_detail['data']['timelines'][0]['intervals'][9]['values']

class Clock(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        # initialize time label
        self.time1 = ''
        self.timeLbl = Label(self, font=('Helvetica', large_text_size), fg="white", bg="black")
        self.timeLbl.pack(side=TOP, anchor=E)
        # initialize day of week
        self.day_of_week1 = ''
        self.dayOWLbl = Label(self, text=self.day_of_week1, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.dayOWLbl.pack(side=TOP, anchor=E)
        # initialize date label
        self.date1 = ''
        self.dateLbl = Label(self, text=self.date1, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.dateLbl.pack(side=TOP, anchor=E)
        self.tick()

    def tick(self):
        if time_format == 12:
            time2 = time.strftime('%I:%M %p') #hour in 12h format
        else:
            time2 = time.strftime('%H:%M') #hour in 24h format

        day_of_week2 = time.strftime('%A')
        date2 = time.strftime(date_format)
        # if time string has changed, update it
        if time2 != self.time1:
            self.time1 = time2
            self.timeLbl.config(text=time2)
        if day_of_week2 != self.day_of_week1:
            self.day_of_week1 = day_of_week2
            self.dayOWLbl.config(text=day_of_week2)
        if date2 != self.date1:
            self.date1 = date2
            self.dateLbl.config(text=date2)
        # calls itself every 200 milliseconds
        # to update the time display as needed
        # could use >200 ms, but display gets jerky
        self.timeLbl.after(200, self.tick)

class Weather(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        self.temperature = ''
        self.forecast = ''
        self.location = ''
        self.currently = ''
        self.icon = ''
        self.degreeFrm = Frame(self, bg="black")
        self.degreeFrm.pack(side=TOP, anchor=W)
        self.temperatureLbl = Label(self.degreeFrm, font=('Helvetica', xlarge_text_size), fg="white", bg="black")
        self.temperatureLbl.pack(side=LEFT, anchor=N)
        self.iconLbl = Label(self.degreeFrm, bg="black")
        self.iconLbl.pack(side=LEFT, anchor=N, padx=20)
        self.currentlyLbl = Label(self, font=('Helvetica', medium_text_size), fg="white", bg="black")
        self.currentlyLbl.pack(side=TOP, anchor=W)

        self.forecastFrm = Frame(self, bg="black")
        self.forecastFrm.pack(side=TOP, anchor=W)
        self.forecastRoll, self.timeRoll, self.iconRoll, self.precRoll, self.tempRoll = [], [], [], [], []

        self.separator = ttk.Separator(self, orient='horizontal')
        self.separator.pack(side=TOP, fill='x')

        self.weeklyFrm = Frame(self, bg='black')
        self.weeklyFrm.pack(side=BOTTOM, anchor=W)
        self.wforecastRoll, self.wtimeRoll, self.wiconRoll, self.wprecRoll, self.wtempRollx, self.wtempRolln = [], [], [], [], [], []

        for i in range(7):
            self.forecastRoll.append(Frame(self.forecastFrm, bg="black"))
            self.forecastRoll[-1].pack(side=LEFT, anchor=W)
            self.wforecastRoll.append(Frame(self.weeklyFrm, bg="black"))
            self.wforecastRoll[-1].pack(side=LEFT, anchor=W)
            self.timeRoll.append(Label(self.forecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.timeRoll[-1].pack(side=BOTTOM)
            self.wtimeRoll.append(Label(self.wforecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.wtimeRoll[-1].pack(side=BOTTOM)
            self.tempRoll.append(Label(self.forecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.tempRoll[-1].pack(side=BOTTOM)
            self.wtempRollx.append(Label(self.wforecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.wtempRollx[-1].pack(side=BOTTOM)
            # self.wtempRolln.append(Label(self.wforecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            # self.wtempRolln[-1].pack(side=BOTTOM)
            self.iconRoll.append(Label(self.forecastRoll[-1], bg="black"))
            self.iconRoll[-1].pack(side=BOTTOM, padx=10)
            self.wiconRoll.append(Label(self.wforecastRoll[-1], bg="black"))
            self.wiconRoll[-1].pack(side=BOTTOM, padx=10)
            self.precRoll.append(Label(self.forecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.precRoll[-1].pack(side=BOTTOM)
            self.wprecRoll.append(Label(self.wforecastRoll[-1], fg="white", bg="black", font=('Helvetica', 10)))
            self.wprecRoll[-1].pack(side=BOTTOM)

        self.get_weather()

    def get_weather(self):
        try:
            update_weather()
            time_current = get_time_as_dt(weather_detail['data']['timelines'][0]['intervals'][0]['startTime']).time()
            time_sunrise = get_time_as_dt(weather_daily['data']['timelines'][0]['intervals'][0]['values']['sunriseTime']).time()
            time_sunset = get_time_as_dt(weather_daily['data']['timelines'][0]['intervals'][0]['values']['sunsetTime']).time()
            daynight = 0
            if time_sunrise>time_current:
                daynight = 1
            elif time_sunset<time_current:
                daynight = 1
            temperature_str = get_temperature_str(weather_detail['data']['timelines'][0]['intervals'][0]['values'])
            summary_str = get_summary_str(weather_detail['data']['timelines'][0]['intervals'][0]['values'])
            icon_path = get_icon_path(weather_detail['data']['timelines'][0]['intervals'][0]['values'])[daynight]

            tps = [1, 2, 3, 5, 7, 10, 13]
            times, icons, precp, temps = [], [], [], []
            for t in tps:
                dic = weather_detail['data']['timelines'][0]['intervals'][t]
                dt = get_time_as_dt(dic['startTime'])
                times.append(dt.hour)
                if times[-1] > 12:
                    times[-1] = '%d PM'%(times[-1]-12)
                else:
                    times[-1] = '%d AM'%times[-1]
                daynight = 0
                if time_sunrise>dt.time():
                    daynight = 1
                elif time_sunset<dt.time():
                    daynight = 1
                icons.append(get_icon_path(dic['values'])[daynight])
                precp.append(round(dic['values']['precipitationProbability']/10)*10)
                if precp[-1] > 30:
                    precp[-1] = '%d %%'%(precp[-1])
                else:
                    precp[-1] = ''
                temps.append(int(round(dic['values']['temperature'])))

            if self.icon != icon_path:
                self.icon = icon_path
                image = Image.open(icon_path)
                image = image.resize((100, 100), Image.ANTIALIAS)
                image = image.convert('RGB')
                photo = ImageTk.PhotoImage(image)
                self.iconLbl.config(image=photo)
                self.iconLbl.image = photo
            if self.currently != summary_str:
                self.currently = summary_str
                self.currentlyLbl.config(text=summary_str)
            if self.temperature != temperature_str:
                self.temperature = temperature_str
                self.temperatureLbl.config(text=temperature_str)
            #if self.location != location2:
            #    if location2 == ", ":
            #        self.location = "Cannot Pinpoint Location"
            #        self.locationLbl.config(text="Cannot Pinpoint Location")
            #    else:
            #        self.location = location2
            #        self.locationLbl.config(text=location2)
            for i in range(len(times)):
                self.timeRoll[i].config(text=times[i])
                self.tempRoll[i].config(text=temps[i])
                self.precRoll[i].config(text=precp[i])
                icon_path = icons[i]
                image = Image.open(icon_path)
                image = image.resize((35, 35), Image.ANTIALIAS)
                image = image.convert('RGB')
                photo = ImageTk.PhotoImage(image)
                self.iconRoll[i].config(image=photo)
                self.iconRoll[i].image = photo

            days, icons, precp, tempsx, tempsn = [], [], [], [], []
            for t in range(7):
                dic = weather_daily['data']['timelines'][0]['intervals'][t]
                dt = get_time_as_dt(dic['startTime'])
                days.append(dt.strftime('%a'))
                icons.append(get_icon_path(dic['values'])[0])
                precp.append(round(dic['values']['precipitationProbability']/10)*10)
                if precp[-1] > 30:
                    precp[-1] = '%d %%'%(precp[-1])
                else:
                    precp[-1] = ''
                tempsx.append(int(round(dic['values']['temperatureMax'])))
                tempsn.append(int(round(dic['values']['temperatureMin'])))

            for i in range(len(days)):
                self.wtimeRoll[i].config(text=days[i])
                self.wtempRollx[i].config(text='%d / %d'%(tempsx[i], tempsn[i]))
                # self.wtempRolln[i].config(text=tempsn[i])
                self.wprecRoll[i].config(text=precp[i])
                icon_path = icons[i]
                image = Image.open(icon_path)
                image = image.resize((35, 35), Image.ANTIALIAS)
                image = image.convert('RGB')
                photo = ImageTk.PhotoImage(image)
                self.wiconRoll[i].config(image=photo)
                self.wiconRoll[i].image = photo

        except Exception as e:
            traceback.print_exc()
            print("Error: %s. Cannot get weather." % e)

        self.after(600000, self.get_weather)

class InfoPane(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.textContainer = Frame(self, bg="black")
        self.textContainer.pack(side=TOP)
        self.get_text()

    def get_text(self):
        try:
            # remove all children
            for widget in self.textContainer.winfo_children():
                widget.destroy()
            # drik panchanga
            masa = pvalue_lookup['masas'][str(panchanga.masa(location_info['date'], location_info['place'])[0])]
            tithi = pvalue_lookup['tithis'][str(panchanga.tithi(location_info['date'], location_info['place'])[0])]
            tithi = tithi.replace(u'Śukla', 'Gaura')
            text = '%s %s %s\n'%(masa, pvalue_lookup["vaisnava"][masa], tithi)#f'{masa} ({pvalue_lookup["vaisnava"][masa]}) {tithi}\n'
            textline = InfoFrame(self.textContainer, text)
            textline.pack(side=TOP, anchor=E)

            today = datetime.datetime.now()
            year, month, day = today.year, today.month, today.day

            for weekday in range(7):
                futureday = today+datetime.timedelta(days=weekday)

                def add_textbox(text):
                    textline = InfoFrame(self.textContainer, text.event+' :' + futureday.strftime('%a'))
                    textline.pack(side=TOP, anchor=E)

                try:
                    events = dates.loc[(futureday.year, futureday.month, futureday.day)]
                    events.apply(add_textbox, axis=1)
                    # initial = futureday.strftime('%a')+': '
                    # strings = []
                    # events.apply(lambda row: strings.append(initial+str(row.event)), axis=1)
                    # textline = InfoFrame(self.textContainer, '\n'.join(strings))
                    # textline.pack(side=TOP, anchor=W)
                except Exception as e:
                    print(e)

            # # MIT shuttle schedule
            # for agency in nbAgencies:
            #     for route in nbRoutes[agency]:
            #         text = nbNames[route] + ': '
            #         predStr = 'http://webservices.nextbus.com/service/publicJSONFeed?command=predictions&a=%s&r=%s&s=%s'%(agency, route, nbStops[agency])
            #         predjson = requests.get(predStr)
            #         pred = json.loads(predjson.content)
            #         if 'direction' in pred['predictions'].keys():
            #             if len(pred['predictions']['direction']['prediction']) > 0:
            #                 text = text + pred['predictions']['direction']['prediction'][0]['minutes']
            #                 if len(pred['predictions']['direction']['prediction']) > 1:
            #                     text = text + ', ' + pred['predictions']['direction']['prediction'][1]['minutes']
            #                 text += 'mins'
            #         else:
            #             text += 'Not running'
            #         textline = InfoFrame(self.textContainer, text)
            #         textline.pack(side=TOP, anchor=E)

        except Exception as e:
            traceback.print_exc()
            print("Error: %s. Error in panchanga." % e)

        self.after(20000, self.get_text)

class InfoFrame(Frame):
    def __init__(self, parent, event_name=""):
        Frame.__init__(self, parent, bg='black')

        self.eventName = event_name
        self.eventNameLbl = Label(self, text=self.eventName, font=('Helvetica', tiny_text_size), fg="white", bg="black")
        self.eventNameLbl.pack(side=RIGHT, anchor=E)

class PicturePane(Frame):
    def __init__(self, parent, *args, **kwargs):
        self.dbx = dropbox.Dropbox(dropbox_api_token)
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.pictureContainer = Frame(self, bg="black")
        self.pictureContainer.pack(side=BOTTOM)
        self.get_pictures()

    def get_pictures(self):
        try:
            # remove all children
            for widget in self.pictureContainer.winfo_children():
                widget.destroy()
            dbx = self.dbx
            files = dbx.files_list_folder('/MirrorPics')
            remote_files = [f.name for f in files.entries]
            local_files = [path.basename(f) for f in glob('./data/photos/*') if not path.isdir(f)]
            for f in remote_files:
                if f not in local_files:
                    dbx.files_download_to_file('./data/photos/%s'%f, '/MirrorPics/%s'%f)
                    print('Downloaded %s'%f)
            for f in local_files:
                if f not in remote_files:
                    os.remove('./data/photos/%s'%f)
                    print('Removed %s'%f)

            pics = [f for f in glob(picfolder+'*') if not path.isdir(f)]
            indices = [0, 0]
            while (indices[0] == indices[1]):
                indices = [randint(0, len(pics)) for x in range(2)]

            for i in range(len(indices)):
                picture = PictureFrame(self.pictureContainer, pics[indices[i]])
                picture.pack(side=LEFT)

        except Exception as e:
            traceback.print_exc()
            print("Error: %s. Cannot get pictures." % e)

        self.after(600*1000, self.get_pictures)

class PictureFrame(Frame):
    def __init__(self, parent, pic=""):
        Frame.__init__(self, parent, bg='black')

        fname = path.basename(pic)
        if path.join(picfolderx, fname) in glob(picfolderx+'*'):
            image = Image.open(path.join(picfolderx, fname))
        else:
            image = Image.open(pic)
            ori = image._getexif().get(274, 1) # 274 is the exif tag for orientation; get this data before transforming the image
            w, h = image.size
            wnew, hnew = int(float(w*imageHeight/h)), imageHeight
            image = image.resize((wnew, hnew), Image.ANTIALIAS)
            image = image.rotate(rot.get(ori, 0))
            image = image.convert('RGB')
            image.save(path.join(picfolderx, fname))
        photo = ImageTk.PhotoImage(image)

        self.picLbl = Label(self, bg="black")
        self.picLbl.config(image=photo)
        self.picLbl.image = photo
        self.picLbl.pack(side=LEFT, padx=10)

class THFrame(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.labelframe = Frame(self, bg='black')
        self.labelframe.pack(side=RIGHT)
        self.Tlabel = Label(self.labelframe, text="Temperature: ", font=('Helvetica', small_text_size), fg="lawn green", bg="black", anchor=W)
        self.Hlabel = Label(self.labelframe, text="Humidity: ", font=('Helvetica', small_text_size), fg="cyan", bg="black", anchor=W)
        self.Tlabel.pack(side=TOP, anchor=W)
        self.Hlabel.pack(side=BOTTOM, anchor=W)
        self.figure = plt.Figure(figsize=(22, 0.7), dpi=100, facecolor='black')
        self.ax = self.figure.add_subplot(1, 1, 1)
        self.ax.patch.set_facecolor('black')
        self.ax2 = self.ax.twinx()
        self.figcanvas = FigureCanvasTkAgg(self.figure, self)
        self.figcanvas.get_tk_widget().pack(side=LEFT)
        self.figcanvas.get_tk_widget().config(bg='black')
        self.read_thlog()
        self.getTH()

    def read_thlog(self):
        try:
            self.n = 0
            with open('./th.size', 'r') as f:
                self.n = int(f.readline().strip('\n'))
            self.thdata = pd.read_csv('th.log', skiprows=range(1, max(0, self.n-1440)), index_col=0, parse_dates=[0], dtype={1 : float, 2 : float})
            #print self.thdata
        except Exception as e:
            traceback.print_exc()
            print("Error: %s. Error reading sensor log file" % e)
            self.thdata = None

    def getTH(self):
        try:
            humidity, temperature = Adafruit_DHT.read_retry(sensor, DHTpin)
            self.n += 1
            ctim = datetime.datetime.now()
            self.Tlabel.config(text='Temperature: %.0f *C'%temperature)
            self.Hlabel.config(text='Humidity: %.2f%%'%humidity)
            print('%s: %.2f RH, %.2fC'%(ctim, humidity, temperature))
            with open('./th.log', 'a') as f:
                f.writelines('%s, %.4f, %.4f\n'%(ctim, temperature, humidity))
            with open('./th.size', 'w') as f:
                f.writelines('%d'%self.n)
            # read csv into dataframe
            # discard topmost entry, and new entry to the bottom
            # save dataframe and then plot it
            try:
                self.thdata.drop(index=[self.thdata.index[0]])
                df = pd.DataFrame(index=[pd.to_datetime(ctim)], columns=['Temperature', 'Humidity'], data=[[temperature, humidity]])
                self.thdata = self.thdata.append(df)
                self.ax.clear()
                self.ax2.clear()
                self.ax2.patch.set_facecolor('black')
                self.ax.patch.set_facecolor('black')
                #print self.thdata['Humidity'] < 100
                self.thdata['Temperature'][self.thdata['Humidity'] < 100].plot(c='lime', ax=self.ax, lw=0.75)
                self.thdata['Humidity'][self.thdata['Humidity'] < 100].plot(c='cyan', ax=self.ax2, lw=0.75)
                self.ax.tick_params(axis='y', colors='lime')
                self.ax2.tick_params(axis='y', colors='cyan')
                #self.ax.tick_params(axis='x', colors='white')
                self.ax.set_ylim(16, 32)
                self.ax2.set_ylim(10, 60)
                plt.setp(self.ax, yticks=[16, 24, 32])
                plt.setp(self.ax2, yticks=[20, 40, 60])
                self.figcanvas.draw()
                #plt.setp(self.ax)
            except Exception as e:
                traceback.print_exc()
                print('Error while plotting.')
        except Exception as e:
            traceback.print_exc()
            print("Error: %s. Cannot read sensor data." % e)

        self.after(60000, self.getTH)

class BottomPane(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.config(bg='black')
        self.title = ''
        self.textLbl = Label(self, text=self.title, font=('Helvetica', medium_text_size), fg="white", bg="black")
        self.textLbl.pack(side=TOP, anchor=W)
        self.textContainer = Frame(self, bg="black")
        self.textContainer.pack(side=TOP)
        self.get_text()

    def get_text(self):
        try:
            # remove all children
            for widget in self.textContainer.winfo_children():
                widget.destroy()
           # if news_country_code == None:
           #     headlines_url = "https://news.google.com/news?ned=us&output=rss"
           # else:
           #     headlines_url = "https://news.google.com/news?ned=%s&output=rss" % news_country_code
            textUrl = "http://docs.google.com/document/export?format=txt&id=1nzzxIMl9TpXi6JRtPbd42TzUYpj7xW9Q23jz5VoTJho&hl=en#"

            contents = requests.get(textUrl)
            lines = contents.content.decode("utf-8-sig").encode("utf-8").split('\r\n')

            for post in lines[-5:]:
                text = post
                textline = BottomText(self.textContainer, text)
                textline.pack(side=TOP, anchor=W)

        except Exception as e:
            traceback.print_exc()
            print("Error: %s. Cannot get text/shuttle predictions." % e)

        self.after(5000, self.get_text)

class BottomText(Frame):
    def __init__(self, parent, event_name=""):
        Frame.__init__(self, parent, bg='black')

        self.eventName = event_name
        self.eventNameLbl = Label(self, text=self.eventName, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.eventNameLbl.pack(side=LEFT, anchor=N)

class Calendar(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        self.title = 'Calendar Events'
        self.calendarLbl = Label(self, text=self.title, font=('Helvetica', medium_text_size), fg="white", bg="black")
        self.calendarLbl.pack(side=TOP, anchor=E)
        self.calendarEventContainer = Frame(self, bg='black')
        self.calendarEventContainer.pack(side=TOP, anchor=E)
        self.get_events()

    def get_events(self):
        #TODO: implement this method
        # reference https://developers.google.com/google-apps/calendar/quickstart/python

        # remove all children
        for widget in self.calendarEventContainer.winfo_children():
            widget.destroy()

        calendar_event = CalendarEvent(self.calendarEventContainer)
        calendar_event.pack(side=TOP, anchor=E)
        pass

class CalendarEvent(Frame):
    def __init__(self, parent, event_name="Event 1"):
        Frame.__init__(self, parent, bg='black')
        self.eventName = event_name
        self.eventNameLbl = Label(self, text=self.eventName, font=('Helvetica', small_text_size), fg="white", bg="black")
        self.eventNameLbl.pack(side=TOP, anchor=E)

class FullscreenWindow:

    def __init__(self):
        self.tk = Tk()
        self.tk.configure(background='black', cursor='none')
        self.topFrame = Frame(self.tk, background = 'black')
        self.bottomFrame = Frame(self.tk, background = 'black')
        self.topFrame.pack(side = TOP, fill=BOTH, expand = YES)
        self.bottomFrame.pack(side = BOTTOM, fill=BOTH, expand = YES)
        self.state = False
        self.tk.bind("<Return>", self.toggle_fullscreen)
        self.tk.bind("<Escape>", self.end_fullscreen)
        # information pane
        # left (weather)
        # weather
        self.weather = Weather(self.topFrame)
        self.weather.pack(side=LEFT, anchor=N, padx=0, pady=0)
        # right (clock, shuttle schedule)
        self.rightInfoFrm = Frame(self.topFrame, bg="black")
        self.rightInfoFrm.pack(side=RIGHT, anchor=N)
        # clock
        self.clock = Clock(self.rightInfoFrm)
        self.clock.pack(side=TOP, anchor=E, padx=0, pady=0)
        # info
        if show_info_pane:
            self.info = InfoPane(self.rightInfoFrm)
            self.info.pack(side=TOP, padx=0, pady=10)
        # bs
        if dh:
            self.thframe = THFrame(self.bottomFrame)
            self.thframe.pack(side=BOTTOM, anchor=S)
        if show_pictures_frame:
            self.pics = PicturePane(self.bottomFrame)
            self.pics.pack(side=BOTTOM, anchor=S)
        # calender
        # self.calender = Calendar(self.bottomFrame)
        # self.calender.pack(side = RIGHT, anchor=S, padx=100, pady=60)

        # start with full screen
        self.toggle_fullscreen()

    def toggle_fullscreen(self, event=None):
        self.state = not self.state  # Just toggling the boolean
        self.tk.attributes("-fullscreen", self.state)
        return "break"

    def end_fullscreen(self, event=None):
        self.state = False
        self.tk.attributes("-fullscreen", False)
        return "break"

if __name__ == '__main__':
    w = FullscreenWindow()
    w.tk.mainloop()
