from gpiozero import LED
from time import sleep

LEFT = 26
RIGHT = 5
UP = 13
DOWN = 21
MENU = 6
POWER = 19

def blink(pin, delay=0.01):
    try:
        btn = LED(pin, active_high=False, initial_value=False)
        btn.blink(on_time=delay, off_time=0.001, n=1, background=False)
        btn.close()
    except:
        print 'Pin already active.'
    return

def play(stream):
    for pin in stream:
        blink(pin)
        sleep(1)
    return

str_blue_light_off = [MENU, UP, UP, RIGHT]
str_blue_light_ON = [MENU, UP, RIGHT]

